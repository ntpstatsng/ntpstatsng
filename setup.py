from setuptools import setup


setup(
    entry_points = {
        'console_scripts': [
            'ntpstatsng = ntpstatsng.main:main'
        ],
    }
)
