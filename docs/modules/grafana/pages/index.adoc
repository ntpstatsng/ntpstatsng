= Grafana
:author:    WOLfgang Schricker
:email:     time@wols.org
include::ROOT:partial$variables.adoc[]
// NO empty line before!

== Datasource Elasticsearch

include::ecs:partial$fields.adoc[leveloffset=0]

[NOTE]
====
*Grafana does not currently (8.4.5) support nested fields.*

[Feature Request] No option for 'nested' type bucket aggregation to deal with nested fields <<grafana_4693>>
====

****
In order to be able to use Grafana, we have a compromise for xref:logstash:index.adoc#_elastic_common_schema_ecs[Logstash] and xref:elasticsearch:index.adoc#_compromise[Elasticsearch].
****

.Upcoming feature
[TIP]
====
ElasticSearch : Add Nested Query Support <<grafana_47233>>
====

[bibliography]
== Bibliography

include::{dir-bib}ecs2.adoc[]
include::{dir-bib}ecs3.adoc[]
include::{dir-bib}grafana_4693.adoc[]
include::{dir-bib}grafana_47233.adoc[]

//

include::ROOT:partial$footer.adoc[]

// ntpstatsng/antora/modules/grafana/pages/index.adoc
