= NTP : Network Time Protocol
:author:    WOLfgang Schricker
:email:     time@wols.org
include::ROOT:partial$variables.adoc[]
// NO empty line before!

== Source, Server, Client



// TODO image



== Data sources

=== ntpd

.revision date
[NOTE]
====
2014-01-31
====

.`peerstats`
Record peer statistics.
Each NTP packet or reference clock update received appends one line to the peerstats file set:

[.nowrap]#`48773 10847.650 127.127.4.1 9714 -0.001605376 0.000000000 0.001424877 0.000958674`#

.`peerstats`
|===
| Item           | Units | Description

| `48773`        | MJD   | date
| `10847.650`    | s     | time past midnight
| `127.127.4.1`  | IP    | source address
| `9714`         | hex   | <<ntp_statusword, status word>>
| `-0.001605376` | s     | clock offset
| `0.000000000`  | s     | roundtrip delay
| `0.001424877`  | s     | dispersion
| `0.000958674`  | s     | RMS jitter
|===

=== chronyd

.revision date
[NOTE]
====
TODO
====



// TODO



=== Meinberg SyncMon(R)

.revision date
[NOTE]
====
{docdate}
====

.`syncmon`
// TODO

[.nowrap]#`TODO`#

.`syncmon`
|===
| Item           | Units | Description

|===

[bibliography]
== Bibliography

* [[[ntp_peerstats]]] University of Delaware (1992-2015): Monitoring Commands and Options : File Set Types, +
[online] link:https://www.eecis.udel.edu/~mills/ntp/html/monopt.html#types[window="_blank"] [2022-04-05T07:57:00].
include::{dir-bib}ntp_statusword.adoc[]
* [[[mbg_syncmon]]] MEINBERG Funkuhren GmbH & Co. KG (2022): IMS Measurement Modules, +
[online] link:https://www.meinbergglobal.com/english/products/ims-measurement-modules.htm[window="_blank"] [2022-04-05T07:51:00].

//

include::ROOT:partial$footer.adoc[]

// ntpstatsng/antora/modules/ntp/pages/index.adoc
