#  ntpstats.conf
#
#  Copyright 2021 WOLfgang Schricker <time@wols.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
#  Required ntpd configuration:
#
#  /etc/ntp.conf
#  statsdir    /var/log/ntp/stats/
#  filegen     loopstats file localhost.localdomain.loopstats
#  filegen     peerstats file localhost.localdomain.peerstats
#
#  Custom fields are capitalized 'NTP.field_name':
#  https://www.elastic.co/guide/en/ecs/current/ecs-custom-fields-in-ecs.html
#
#  Currend supported event datasets:
#      loopstats
#      peerstats
#
#  /etc/logstash/pipeline.yml
#  - pipeline.id: ntpstats
#    path.config: "/etc/logstash/conf.d/ntpstats.conf"

input {
    file {
        path      => ["/var/log/ntp/stats/*loopstats"]
        add_field => {
            "event.module"               => "ntpd"
            "event.kind"                 => "metric"
            "event.category"             => ["process"]
            "event.type"                 => ["info"]
            "event.provider"             => "ntpd"
            "event.dataset"              => "ntpd.loopstats"
            "event.reference"            => "https://www.eecis.udel.edu/~mills/ntp/html/monopt.html#loopstats"
            "data_stream.type"           => "info"
            "data_stream.dataset"        => "ntpd.loopstats"
            "data_stream.namespace"      => "private"
            "[@metadata][es_index_name]" => "ntpstats"
        }
    }

    file {
        path      => ["/var/log/ntp/stats/*peerstats"]
        add_field => {
            "event.module"               => "ntpd"
            "event.kind"                 => "metric"
            "event.category"             => ["network"]
            "event.type"                 => ["connection"]
            "event.provider"             => "ntpd"
            "event.dataset"              => "ntpd.peerstats"
            "event.reference"            => "https://www.eecis.udel.edu/~mills/ntp/html/monopt.html#peerstats"
            "data_stream.type"           => "connection"
            "data_stream.dataset"        => "ntpd.peerstats"
            "data_stream.namespace"      => "private"
            "[@metadata][es_index_name]" => "ntpstats"
        }
    }
}

filter {
    mutate {
        add_field => {
            "event.ingested"               => "%{@timestamp}"
            "event.timezone"               => "UTC"
            "ecs.version"                  => "1.11.0"
            "network.application"          => "ntp"
            "network.protocol"             => "ntp"
            "network.transport"            => "udp"
            "service.type"                 => "ntp"
            "source.address"               => "localhost"
            "[@metadata][es_index_suffix]" => "%{+YYYYMMdd}"
        }
    }

    if [path] {
        # /var/log/ntp/stats/localhost.localdomain.loopstats
        #     split(/).array[-1]
        # localhost.localdomain.loopstats
        #     split(.)
        # [localhost, localdomain, loopstats]
        #     pop(1)
        # [localhost, localdomain]
        #     join(.)
        # localhost.localdomain

        ruby {
            code => "
                file = event.get('path').split('/')
                file = file[-1].split('.')
                file.pop
                event.set('source.address', file.join(sep='.'))
            "
            remove_field => ["path"]
        }
    }

    if [event.dataset] == "ntpd.loopstats" {
        csv {
            separator => " "
            columns   => [
                "ntp.mjd",
                "ntp.time_past_midnight",
                "ntp.clock_offset",
                "ntp.frequency_offset",
                "ntp.rms_jitter",
                "ntp.frequency_jitter",
                "ntp.loop_time_constant"
            ]
        }
    }

    if [event.dataset] == "ntpd.peerstats" {
        csv {
            separator => " "
            columns   => [
                "ntp.mjd",
                "ntp.time_past_midnight",
                "destination.ip",
                "ntp.status_word",
                "ntp.clock_offset",
                "ntp.delay",
                "ntp.dispersion",
                "ntp.jitter"
            ]
            add_field => {
                "destination.port" => 123
            }
        }
    }

    mutate {
        convert => {
            "destination.port"       => "integer"
            "ntp.clock_offset"       => "float"
            "ntp.delay"              => "float"
            "ntp.dispersion"         => "float"
            "ntp.frequency_jitter"   => "float"
            "ntp.frequency_offset"   => "float"
            "ntp.jitter"             => "float"
            "ntp.mjd"                => "integer"
            "ntp.rms_jitter"         => "float"
            "ntp.time_past_midnight" => "float"
        }
        rename  => {
            "message" => "event.original"
        }
    }

    ruby {
        code => "
            # convert 'mjd' + 'time_past_midnight'
            statsdate           = Date.jd(2400001 + event.get('ntp.mjd'))
            statssec, statsmsec = event.get('ntp.time_past_midnight').to_s.split('.',2)
            statstime           = Time.parse(statsdate.to_s).utc + statssec.to_i

            # replace '@timestamp'
            event.set('@timestamp', LogStash::Timestamp.new(statstime))
            # update 'es_index_suffix'
            event.set('[@metadata][es_index_suffix]', statstime.strftime('%Y%m%d'))
        "
        add_field => {
            "[@metadata][es_index]" => "%{[@metadata][es_index_name]}.%{[@metadata][es_index_suffix]}"
        }
    }
}

output {
    # DEBUG
    #file {
    #    path => "/tmp/logstash-ntpstats-%{+YYYYMMdd}.json"
    #    # write_behavior => "overwrite"
    #}

    if "_rubyexception" in [tags] {
        stdout {
            codec => rubydebug {
                metadata => "true"
            }
        }

        file {
            path => "/tmp/logstash-ntpstats_failure.json"
        }
    } else {
        pipeline {
            send_to => ["es-cluster"]
        }
    }
}
