== NTP statistics - Next Generation

link:https://ntpstatsng.gitlab.io/ntpstatsng/ntpstatsng/main/[window="_blank"]

[IMPORTANT]
====
This software is under the heavy development and considered ALPHA quality till the version hits `>= 1.0.0`.
Things might be broken, not all features have been implemented and APIs will be likely to change.

YOU HAVE BEEN WARNED.
====

// ntpstatsng/docs/modules/ROOT/partials/readme.adoc
