"""NTP statistics - Next Generation."""

import os

__all__ = ('__version__', 'ntpstatsng')


"""Set package version from file ntpstatsng/VERSION."""
with open(os.path.join(os.path.dirname(__file__), 'VERSION')) as __file:
    __version__ = __file.read().strip()
